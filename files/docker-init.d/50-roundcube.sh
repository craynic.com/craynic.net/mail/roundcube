#!/usr/bin/env bash

set -Eeuo pipefail

(
  BUILD_TS=$(cat /build-timestamp)

  : "${ROUNDCUBEMAIL_DES_KEY:=$(head /dev/urandom | base64 | head -c 24)}"

  cat -- config/config.inc.php.craynic

  echo "\$config['db_dsnw'] = '${ROUNDCUBEMAIL_DB_DSN}';";
  echo "\$config['imap_host'] = '${ROUNDCUBEMAIL_IMAP_HOST}';";
  echo "\$config['smtp_host'] = '${ROUNDCUBEMAIL_SMTP_HOST}';";
  echo "\$config['des_key'] = '${ROUNDCUBEMAIL_DES_KEY}';";
  echo "\$config['temp_dir'] = sys_get_temp_dir();";

  ( [ -n "$ROUNDCUBEMAIL_PRODUCT_NAME" ] && echo "\$config['product_name'] = '$ROUNDCUBEMAIL_PRODUCT_NAME';" ) || true
  ( [ -n "$ROUNDCUBEMAIL_SUPPORT_URL" ] && echo "\$config['support_url'] = '$ROUNDCUBEMAIL_SUPPORT_URL';" ) || true
  ( [ -n "$ROUNDCUBEMAIL_SKIN_LOGO" ] && echo "\$config['skin_logo'] = '$ROUNDCUBEMAIL_SKIN_LOGO?$BUILD_TS';" ) || true

  ( [ -n "$ROUNDCUBEMAIL_CRAYNIC_API_ENDPOINT" ] \
  && echo "\$config['password_craynic_api_endpoint'] = '$ROUNDCUBEMAIL_CRAYNIC_API_ENDPOINT';" ) || true

  (ls config/config.d/*.php 2>/dev/null || true) | while read -r file; do
    echo "require_once __DIR__ . '/../$file';"
  done

  ( [ -n "$ROUNDCUBEMAIL_LOGOUT_REDIRECT_URL" ] \
  && echo "\$config['craynic_logout_redirect_url'] = '$ROUNDCUBEMAIL_LOGOUT_REDIRECT_URL';" ) || true
) > config/config.inc.php

(
  ( [ -n "${ROUNDCUBEMAIL_UPLOAD_MAX_FILESIZE}" ] \
    && echo "upload_max_filesize=${ROUNDCUBEMAIL_UPLOAD_MAX_FILESIZE}" \
    && echo "post_max_size=${ROUNDCUBEMAIL_UPLOAD_MAX_FILESIZE}"
    ) || true
) >> /usr/local/etc/php/conf.d/roundcube.ini

DB_HOST=$(php -r "\$parsed = parse_url('${ROUNDCUBEMAIL_DB_DSN}');
echo sprintf('%s:%d', \$parsed['host'], \$parsed['port'] ?? 3306);"
);

wait-for-it.sh "$DB_HOST" -t 30
bin/initdb.sh --dir=SQL --create 2>/dev/null || bin/updatedb.sh --dir=SQL --package=roundcube
