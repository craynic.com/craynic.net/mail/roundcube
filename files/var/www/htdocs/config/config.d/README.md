Keep this file otherwise Git would not include the empty `config.d` directory in the index.

The `config.d` directory is intended for additional configuration files to be included in the main `config.inc.php`
file.
