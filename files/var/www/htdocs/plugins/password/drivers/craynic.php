<?php

/** @noinspection PhpUndefinedConstantInspection */
/** @noinspection PhpUnused */

class rcube_craynic_password
{
    function save($currentPassword, $newPassword): array|int
    {
        try {
            if (!isset($_SESSION['oauth_token'])) {
                return PASSWORD_CONNECT_ERROR;
            }

            return $this->makeCall([
                'currentPassword' => $currentPassword,
                'newPassword' => $newPassword,
            ]);
        } catch (Throwable $exception) {
            return match ($exception->getCode()) {
                PASSWORD_CRYPT_ERROR,
                    PASSWORD_ERROR,
                    PASSWORD_CONNECT_ERROR,
                    PASSWORD_IN_HISTORY,
                    PASSWORD_CONSTRAINT_VIOLATION,
                    PASSWORD_COMPARE_CURRENT,
                    PASSWORD_COMPARE_NEW => $exception->getCode(),
                default => PASSWORD_ERROR,
            };
        }
    }

    private function makeCall(array $postFields): array|int {
        /** @noinspection PhpUndefinedClassInspection */
        $rcmail = rcmail::get_instance();
        $apiEndpoint = $rcmail->config->get('password_craynic_api_endpoint');

        if (empty($apiEndpoint)) {
            throw new RuntimeException('API endpoint not defined.', PASSWORD_ERROR);
        }

        $oauthSslVerify = (bool) $rcmail->config->get('oauth_verify_peer', true);

        $options = [
            CURLOPT_URL => sprintf('%s/account/password', $apiEndpoint),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => $oauthSslVerify,
            CURLOPT_SSL_VERIFYHOST => $oauthSslVerify,
            CURLOPT_HTTPHEADER => [
                sprintf(
                    'Authorization: %s %s',
                    $_SESSION['oauth_token']['token_type'],
                    $_SESSION['oauth_token']['access_token']
                ),
            ],
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postFields,
        ];

        // Call GET to fetch values from server
        $curl = curl_init();
        curl_setopt_array($curl, $options);

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($err) {
            throw new RuntimeException($err, PASSWORD_CONNECT_ERROR);
        }

        if ($info['http_code'] === 200) {
            return PASSWORD_SUCCESS;
        } elseif ($info['http_code'] === 403) {
            $decodedResponse = json_decode($response, JSON_OBJECT_AS_ARRAY);
            if (!is_array($decodedResponse) || !isset($decodedResponse['status'])) {
                return PASSWORD_CONNECT_ERROR;
            }

            return $this->apiErrorToPasswordError($decodedResponse['status']);
        } else {
            throw new RuntimeException('Unknown response.', PASSWORD_CONNECT_ERROR);
        }
    }

    private function apiErrorToPasswordError(string $apiError): array|int
    {
        /** @noinspection PhpUndefinedClassInspection */
        $rcube = rcube::get_instance();

        return match ($apiError) {
            'ERROR_WRONG_CURRENT_PASSWORD' => [
                'code' => PASSWORD_COMPARE_CURRENT,
                'message' => $rcube->gettext('passwordincorrect', 'password'),
            ],
            'ERROR_WEAK_NEW_PASSWORD' => PASSWORD_CONSTRAINT_VIOLATION,
            'ERROR_COMPROMISED_PASSWORD' => [
                'code' => PASSWORD_ERROR,
                'message' => $rcube->gettext('craynic.passwordiscompromised'),
            ],
            'ERROR_PASSWORDS_ARE_SAME' => [
                'code' => PASSWORD_ERROR,
                'message' => $rcube->gettext('samepasswd', 'password'),
            ],
            default => PASSWORD_ERROR,
        };
    }

    public function compare()
    {
        // this driver makes no comparison - let the API server do the job
        return null;
    }

    public function strength_rules(): array|string
    {
        /** @noinspection PhpUndefinedClassInspection */
        $rcube = rcube::get_instance();

        return [
            $rcube->gettext('password_requirements', 'craynic'),
            sprintf(
                $rcube->gettext('craynic.password_must_not_be_pwnd'),
                'https://haveibeenpwned.com/'
            )
        ];
    }
}
