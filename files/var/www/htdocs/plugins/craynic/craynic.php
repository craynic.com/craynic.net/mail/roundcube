<?php

/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedClassInspection */

/**
 * @method add_hook(string $event, callable $callback)
 * @method load_config(string $fname = 'config.inc.php')
 * @method add_texts(string $dir)
 */
final class craynic extends rcube_plugin
{
    private ?rcmail $rc = null;

    public function init(): void
    {
        $this->rc = rcmail::get_instance();
        $this->load_config();

        if ($this->rc->task == 'settings') {
            $this->add_texts('localization/');
        }

        $this->add_hook('logout_after', [$this, 'logout_after']);
        $this->add_hook('render_folder_selector', [$this, 'render_folder_selector']);
        $this->add_hook('render_mailboxlist', [$this, 'render_mailboxlist']);
        $this->add_hook('password_change', [$this, 'password_change']);
    }

    public function password_change(array $data): array
    {
        // do not allow to update the password stored in SESSION after password change
        // if the password driver is craynic
        // - the password in SESSION is an OAuth2 access token which should not change
        if ((string) $this->rc->config->get('password_driver') === 'craynic') {
            $data['new_pass'] = $this->rc->decrypt($_SESSION['password']);
        }

        return $data;
    }

    public function logout_after(): void
    {
        $redirectUrl = (string) $this->rc->config->get('craynic_logout_redirect_url');

        if ($redirectUrl !== '') {
            header(sprintf('Location: %s', $redirectUrl), true, 303);
        }
    }

    public function render_folder_selector(array $data): array
    {
        if ($data['list']['virtual']['virtual']) {
            unset($data['list']['virtual']);
        }

        return $data;
    }

    public function render_mailboxlist(array $data): array
    {
        $translations = [
            'virtual' => [
                'en' => 'Virtual folders',
                'cs_CZ' => 'Virtuální složky',
            ],
            'virtual/Flagged' => [
                'en' => 'Flagged items',
                'cs_CZ' => 'Označené položky',
            ],
        ];

        $lang = $this->rc->config->get('language');

        if ($data['list']['virtual']['virtual'] ?? false) {
            $data['list']['virtual']['name'] = $translations['virtual'][$lang] ?? $translations['virtual']['en'];

            if (isset($data['list']['virtual']['folders']['Flagged'])) {
                $data['list']['virtual']['folders']['Flagged']['name'] =
                    $translations['virtual/Flagged'][$lang] ?? $translations['virtual/Flagged']['en'];
            }
        }

        return $data;
    }
}
