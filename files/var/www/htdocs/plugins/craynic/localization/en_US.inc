<?php

/** @noinspection HtmlUnknownTarget */

$messages['password_requirements'] = 'The new password must meet at least 4 out of the following 5 criteria:'
    . ' the password must contain an uppercase letter, a lowercase letter, a digit, a special character'
    . ' and must be longer than 12 characters.';
$messages['password_must_not_be_pwnd'] = 'The password'
    . ' <a href="%01s" target="_blank">must not be compromised</a>.';
