<?php

/** @noinspection HtmlUnknownTarget */

$messages['password_requirements'] = 'Heslo musí splňovat alespoň 4 z následujících 5 kritérií:'
    . ' heslo musí obsahovat velké písmeno, malé písmeno, číslo, speciální znak a být delší než 12 znaků.';
$messages['password_must_not_be_pwnd'] = 'Heslo se nesmí nacházet'
    . ' v <a href="%01s" target="_blank">databázi kompromitovaných hesel</a>.';
