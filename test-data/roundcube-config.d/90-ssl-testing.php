<?php
$config['imap_conn_options'] = [
    'ssl' => [
        'verify_peer_name' => false,
        'allow_self_signed' => true,
    ],
];

$config['managesieve_conn_options'] = [
    'ssl' => [
        'verify_peer_name' => false,
        'allow_self_signed' => true,
    ],
];

$config['smtp_conn_options'] = [
    'ssl' => [
        'verify_peer_name' => false,
        'allow_self_signed' => true,
    ],
];

