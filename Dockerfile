FROM registry.gitlab.com/craynic.com/docker/lap:13.3.4-php8.2@sha256:ce5d6716bd25b91cd2d1f1f52750ec3396dcb50a16da4f98600ef5e21096b7b9

# renovate: datasource=github-releases depName=roundcube/roundcubemail depType=dependencies versioning=loose
ARG ROUNDCUBEMAIL_VERSION="1.6.10"
ARG DOWNLOAD_URI="https://github.com/roundcube/roundcubemail/releases/download/\
${ROUNDCUBEMAIL_VERSION}/roundcubemail-${ROUNDCUBEMAIL_VERSION}-complete.tar.gz"
ARG DOWNLOAD_FILENAME="/tmp/roundcubemail.tar.gz"
ARG GPG_KEY="F3E4C04BB3DB5D4215C45F7F5AB2BAA141C4F7D5"
ARG GNUPGHOME=""
ARG COMPOSER_ALLOW_SUPERUSER=1

COPY files/ /

RUN apk add --no-cache --virtual .build-deps \
        git~=2 \
        gpg~=2 \
        gpg-agent~=2 \
        dirmngr~=2 \
        unzip~=6 \
        git~=2 \
    && curl -o "$DOWNLOAD_FILENAME" -fSL --no-progress-meter "$DOWNLOAD_URI" \
  	&& curl -o "$DOWNLOAD_FILENAME.asc" -fSL --no-progress-meter "$DOWNLOAD_URI.asc" \
    && GNUPGHOME="$(mktemp -d)" \
	  && ( gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$GPG_KEY" \
	    || gpg --batch --keyserver pgp.mit.edu --recv-keys "$GPG_KEY" ) \
    && gpg --batch --verify "$DOWNLOAD_FILENAME.asc" "$DOWNLOAD_FILENAME" \
    && gpgconf --kill all \
    && tar -xf "$DOWNLOAD_FILENAME" --strip-components=1 --no-same-owner \
    && rm -rf -- "$GNUPGHOME" "$DOWNLOAD_FILENAME.asc" "$DOWNLOAD_FILENAME" "installer" \
    && install-php-extensions \
      ldap \
    && composer require --update-no-dev \
        johndoh/contextmenu \
        kitist/html5_notifier \
    && chown -R www-data:www-data . \
    && chmod -R go-w . \
    && apk del --no-cache .build-deps \
    && curl -fL --no-progress-meter https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh \
        > /usr/local/sbin/wait-for-it.sh \
    && chmod +x /usr/local/sbin/wait-for-it.sh

ENV ROUNDCUBEMAIL_SKIN_LOGO="images/logo.png" \
    ROUNDCUBEMAIL_IMAP_HOST="tls://localhost:143" \
    ROUNDCUBEMAIL_SMTP_HOST="tls://localhost:587" \
    ROUNDCUBEMAIL_DB_DSN="" \
    ROUNDCUBEMAIL_DES_KEY="" \
    ROUNDCUBEMAIL_PRODUCT_NAME="" \
    ROUNDCUBEMAIL_SUPPORT_URL="" \
    ROUNDCUBEMAIL_UPLOAD_MAX_FILESIZE="50M" \
    ROUNDCUBEMAIL_PASSWORD_CRAYNIC_API_ENDPOINT="" \
    ROUNDCUBEMAIL_LOGOUT_REDIRECT_URL="" \
    LAP_MAX_EXECUTION_TIME=600 \
    LAP_PHP_MAX_MEMORY="64M"

RUN date +%s%N > /build-timestamp
